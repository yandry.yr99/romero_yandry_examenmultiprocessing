import time
import numpy as np
import functools
import multiprocessing

 

def how_many_within_range_sequential(row,minimum,maximum): 
    count = 0
    for n in row:
        if minimum <= n <= maximum:
            count += 1
    return count
 
# Complete the how_many_within_range_parallel function below.

def how_many_within_range_parallel(row, minimum, maximum):
    count = 0
    for n in row:
        if minimum <= n <= maximum:
            count += 1
    return count


 

if __name__ == '__main__':

    # Prepare data
    np.random.RandomState(100)
    arr = np.random.randint(0, 10, size=[4000000, 10])
    ar = arr.tolist()
   
    inicioSec = time.time()
    resultsSec = []
    for row in ar:
        resultsSec.append(how_many_within_range_sequential(row, minimum=4, maximum=8))
    finSec =  time.time()

   
    # You can modify this to adapt to your code

    inicioPar = time.time()   

    resultsPar = []

    for row in ar:
        process = multiprocessing.Process(target= how_many_within_range_sequential, args=(row,4,8))
        resultsPar.append(process)
        process.start()
        process.join()
    finPar = time.time()   

    

    ##print('Results are correct!\n' if functools.reduce(lambda x, y : x and y, map(lambda p, q: p == q,resultsSec,resultsPar), True) else 'Results are incorrect!\n')

    print('Sequential Process took %.3f ms \n' % ((finSec - inicioSec)*1000))

    print('Parallel Process took %.3f ms \n' % ((finPar - inicioPar)*1000))